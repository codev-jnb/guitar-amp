#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>
#include <BluetoothSerial.h>
#include <BluetoothA2DPSource.h>

// WiFi credentials
const char* ssid = "ESP32_Access_Point";
const char* password = "123456789";

// Création d'une instance AsyncWebServer sur le port 80
AsyncWebServer server(80);
// Création d'une instance WebSocket
AsyncWebSocket ws("/ws");

const char html_page[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>Bluetooth Device Connect</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function scanBluetooth() {
      fetch('/scan')
        .then(response => response.json())
        .then(data => {
          let devicesList = document.getElementById('devices');
          devicesList.innerHTML = '';
          data.devices.forEach(device => {
            let option = document.createElement('option');
            option.value = device.address;
            option.innerText = device.name + ' (' + device.address + ')';
            devicesList.appendChild(option);
          });
        });
    }
  </script>
</head>
<body>
  <h2>Connect to Bluetooth Device</h2>
  <form action="/connect" method="get">
    Bluetooth Device Address: 
    <select id="devices" name="bt_device">
      <option value="" disabled selected>Select a device</option>
    </select><br>
    <input type="submit" value="Connect">
  </form>
  <button onclick="scanBluetooth()">Scan Bluetooth Devices</button>
</body>
</html>
)rawliteral";

void setup() {
    Serial.begin(115200);
    
    // Set up WiFi in AP mode with a fixed IP
    WiFi.softAP(ssid, password);
    IPAddress local_ip(192, 168, 0, 1);
    IPAddress gateway(192, 168, 0, 1);
    IPAddress subnet(255, 255, 255, 0);
    WiFi.softAPConfig(local_ip, gateway, subnet);

    Serial.println("WiFi Access Point started");
    Serial.print("IP Address: ");
    Serial.println(WiFi.softAPIP());

    // Serve HTML content at the root
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send_P(200, "text/html", html_page);
    });

    // Start server
    server.begin();
    Serial.println("HTTP server started");
}

void loop() {
    // Gestion des connexions WebSocket
    ws.cleanupClients();
}
