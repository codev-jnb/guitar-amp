#include "BluetoothA2DPSource.h"
#include <driver/adc.h>
#include "esp_adc_cal.h"

BluetoothA2DPSource ad2p_source;

esp_bd_addr_t peer_address = {0x4C, 0x87, 0x5D, 0xAB, 0x51, 0x73}; // Adresse MAC d'un casque audio, à modifier en fonction du périphérique

// ADC channel for analog input
#define ADC_CHANNEL_LEFT ADC1_CHANNEL_0 // GPIO 36
#define ADC_CHANNEL_RIGHT ADC1_CHANNEL_1 // GPIO 37

// Number of samples per frame
#define SAMPLES_PER_FRAME 256

// Configure ADC for left channel
adc1_config_width(ADC_WIDTH_BIT_12); // 12-bit resolution
adc1_config_channel_atten(ADC_CHANNEL_LEFT, ADC_ATTEN_DB_11); // Attenuation to 11dB

// Configure ADC for right channel
adc1_config_width(ADC_WIDTH_BIT_12); // 12-bit resolution
adc1_config_channel_atten(ADC_CHANNEL_RIGHT, ADC_ATTEN_DB_11); // Attenuation to 11dB

// Buffer to hold the ADC samples
int16_t adcBufferLeft[SAMPLES_PER_FRAME]; // Left channel
int16_t adcBufferRight[SAMPLES_PER_FRAME]; // Right channel

// ADC calibration characteristics
esp_adc_cal_characteristics_t *adc_chars;

// callback 
int32_t get_sound_data(Frame *data, int32_t frameCount) {
    for (int i = 0; i < frameCount; i++) {
        // Read from left ADC channel
        int adc_value_left = adc1_get_raw(ADC_CHANNEL_LEFT);
        // Convert the left ADC value to the range expected by the audio data
        adcBufferLeft[i] = (adc_value_left - 2048) * 16; // Center around 0 and amplify

        // Read from right ADC channel
        int adc_value_right = adc1_get_raw(ADC_CHANNEL_RIGHT);
        // Convert the right ADC value to the range expected by the audio data
        adcBufferRight[i] = (adc_value_right - 2048) * 16; // Center around 0 and amplify
    }

    // Copy the buffer for each channel to the frame data
    for (int i = 0; i < frameCount; i++) {
        // Left channel
        data->left_data[i] = adcBufferLeft[i];
        // Right channel
        data->right_data[i] = adcBufferRight[i];
    }

    return frameCount;
}




void setup() {
  Serial.begin(115200);
  a2dp_source.set_auto_reconnect(true);
  // Allocate memory for the ADC calibration characteristics
  adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));

  // Characterize ADC
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTENUATION, ADC_READ_RESOLUTION, 1100, adc_chars);

  a2dp_source.start("ESP32", get_sound_data);
  a2dp_source.set_volume(100);
  a2dp_source.connect_to(peer_address);
  a2dp_source.write_data(data);
  }


void loop() {
}
