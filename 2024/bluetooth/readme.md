# Streaming Audio Bluetooth avec ESP32

Dans ce dossier vous trouverez les codes permettant d'établir une connection Bluetooth a2dp (audio), réalisés sur l'*IDE Arduino*.
Ils ont nécessité les bibliothèques [BluetoothA2DP](https://github.com/pschatzmann/ESP32-A2DP) et [Audiotools](https://github.com/pschatzmann/arduino-audio-tools).

Détaillons les codes:

## bt_music_sender_write_mono


### Fonctionnalités

Diffusion d'audio à partir d'une entrée analogique vers un appareil Bluetooth.
Reconnexion automatique au dernier appareil appairé.
Nom et volume de l'appareil Bluetooth configurables.

### Pré-requis

Carte de développement ESP32.
Source audio analogique.
Appareil audio Bluetooth appairé (casque, enceinte, etc.).

### Configuration Matérielle

1. Connectez votre source audio analogique à la broche GPIO 36 (ADC1_CHANNEL_0) de l'ESP32.
2. Assurez-vous que votre appareil audio Bluetooth est détectable et prêt à être appairé.

### Configuration Logicielle

Importez les bibliothèques nécessaires :
- `BluetoothA2DPSource.h`
- `driver/adc.h`
- `esp_adc_cal.h`

Mettez à jour l'adresse MAC Bluetooth dans le code pour qu'elle corresponde à votre appareil audio Bluetooth :

```cpp
esp_bd_addr_t peer_address = {0x4C, 0x87, 0x5D, 0xAB, 0x51, 0x73}; // Remplacez par l'adresse MAC de votre appareil
```

Vous pouvez trouver l'adresse MAC de vos appareils Bluetooth en utilisant l'application mobile *Serial Bluetooth Terminal* où à l'aide du programme [bt_scan_and_connect](#bt_scan_and_connect).

### Explication du Code
#### Initialisation et Configuration

- **Configuration de l'ADC** : L'ADC est configuré pour lire depuis GPIO 36 avec une résolution de 12 bits et une atténuation de 11 dB.
- **Configuration du Bluetooth** : La source Bluetooth A2DP est initialisée et la reconnexion automatique est activée.

#### Lecture de l'ADC et Traitement des Données Audio

La fonction `get_sound_data` lit des échantillons depuis l'ADC, convertit les lectures brutes de l'ADC en tension, ajuste la tension pour la centrer autour de 0 et remplit un tampon pour la sortie stéréo :

```cpp
int32_t get_sound_data(Frame *data, int32_t frameCount) {
    for (int i = 0; i < frameCount; i++) {
        uint32_t adc_reading = adc1_get_raw(ADC_CHANNEL);
        uint32_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
        int16_t sample = (voltage - 1800) * 16; // Centrer autour de 0 et amplifier
        adcBuffer[i * 2] = sample;      // Canal gauche
        adcBuffer[i * 2 + 1] = sample;  // Canal droit
    }
    memcpy(data, adcBuffer, frameCount * 4);
    return frameCount;
}
```

#### Fonctions Setup et Loop

*setup()* : Initialise la connexion série, configure la calibration de l'ADC, démarre la source Bluetooth A2DP, règle le volume et se connecte à l'appareil Bluetooth.

*loop()* : La boucle principale est vide car toutes les opérations nécessaires sont gérées par la fonction de rappel et la bibliothèque Bluetooth.

```cpp
void setup() {
    Serial.begin(115200);
    a2dp_source.set_auto_reconnect(true);
    adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTENUATION, ADC_READ_RESOLUTION, 1100, adc_chars);
    a2dp_source.start("ESP32", get_sound_data);
    a2dp_source.set_volume(100);
    a2dp_source.connect_to(peer_address);
}

void loop() {
}
```

### Utilisation

1. Compilez et téléversez le code sur votre ESP32.
2. Ouvrez le moniteur série pour observer l'état de la connexion et les messages de debug.
3. Assurez-vous que votre appareil audio Bluetooth est allumé et en mode appairage.
4. L'ESP32 devrait se connecter automatiquement à l'appareil audio Bluetooth et commencer à diffuser de l'audio.

### Dépannage

- **Pas de son** : Assurez-vous que la source audio analogique est correctement connectée à la broche GPIO 36 et fournit un signal valide.
- **Problèmes de connexion** : Vérifiez l'adresse MAC Bluetooth et assurez-vous que l'appareil est en mode appairage. Consultez le moniteur série pour d'éventuels messages d'erreur.
- **Problème de compilation** : il est possible que le programme soit trop lourd pour la carte, il faut donc modifier les partitions. Pour cela, dans l'IDE Arduino, rendez vous dans Tools > "Partition Scheme" > Huge APP.



## bt_music_sender_write_stereo

### Fonctionnalités

- Diffusion d'audio stéréo à partir d'une entrée analogique vers un appareil Bluetooth.
- Reconnexion automatique au dernier appareil appairé.
- Nom et volume de l'appareil Bluetooth configurables.

### Pré-requis

- Carte de développement ESP32.
- Source audio analogique.
- Appareil audio Bluetooth appairé (casque, enceinte, etc.).

### Configuration Matérielle

1. Connectez votre source audio analogique aux broches GPIO 36 (ADC1_CHANNEL_0) et GPIO 37 (ADC1_CHANNEL_1) de l'ESP32 pour les canaux gauche et droit respectivement.
2. Assurez-vous que votre appareil audio Bluetooth est détectable et prêt à être appairé.

### Configuration Logicielle

1. Importez les bibliothèques nécessaires :
- `BluetoothA2DPSource.h`
- `driver/adc.h`
- `esp_adc_cal.h`

2. Mettez à jour l'adresse MAC Bluetooth dans le code pour qu'elle corresponde à votre appareil audio Bluetooth :

```cpp
esp_bd_addr_t peer_address = {0x4C, 0x87, 0x5D, 0xAB, 0x51, 0x73}; // Remplacez par l'adresse MAC de votre appareil
```

Vous pouvez trouver l'adresse MAC de vos appareils Bluetooth en utilisant l'application mobile *Serial Bluetooth Terminal* où à l'aide du programme [bt_scan_and_connect](#bt_scan_and_connect).

### Explication du Code

#### Initialisation et Configuration

- **Configuration de l'ADC** : Les canaux ADC sont configurés pour lire depuis GPIO 36 (canal gauche) et GPIO 37 (canal droit) avec une résolution de 12 bits et une atténuation de 11 dB.
- **Configuration du Bluetooth** : La source Bluetooth A2DP est initialisée et la reconnexion automatique est activée.

#### Lecture de l'ADC et Traitement des Données Audio

La fonction `get_sound_data` lit des échantillons depuis les canaux ADC gauche et droit, convertit les lectures brutes de l'ADC en tension, ajuste la tension pour la centrer autour de 0 et remplit un tampon pour la sortie stéréo :

```cpp
int32_t get_sound_data(Frame *data, int32_t frameCount) {
    for (int i = 0; i < frameCount; i++) {
        // Lire depuis le canal ADC gauche
        int adc_value_left = adc1_get_raw(ADC_CHANNEL_LEFT);
        // Convertir la valeur ADC gauche en une plage attendue par les données audio
        adcBufferLeft[i] = (adc_value_left - 2048) * 16; // Centrer autour de 0 et amplifier

        // Lire depuis le canal ADC droit
        int adc_value_right = adc1_get_raw(ADC_CHANNEL_RIGHT);
        // Convertir la valeur ADC droite en une plage attendue par les données audio
        adcBufferRight[i] = (adc_value_right - 2048) * 16; // Centrer autour de 0 et amplifier
    }

    // Copier le tampon de chaque canal dans les données de la trame
    for (int i = 0; i < frameCount; i++) {
        // Canal gauche
        data->left_data[i] = adcBufferLeft[i];
        // Canal droit
        data->right_data[i] = adcBufferRight[i];
    }

    return frameCount;
}
```

#### Fonctions Setup et Loop

- **setup()** : Initialise la connexion série, configure la calibration de l'ADC, démarre la source Bluetooth A2DP, règle le volume et se connecte à l'appareil Bluetooth.
- **loop()** : La boucle principale est vide car toutes les opérations nécessaires sont gérées par la fonction de rappel et la bibliothèque Bluetooth.

```cpp
void setup() {
    Serial.begin(115200);
    a2dp_source.set_auto_reconnect(true);
    adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTENUATION, ADC_READ_RESOLUTION, 1100, adc_chars);
    a2dp_source.start("ESP32", get_sound_data);
    a2dp_source.set_volume(100);
    a2dp_source.connect_to(peer_address);
}

void loop() {
}
```

### Utilisation

1. Compilez et téléversez le code sur votre ESP32.
2. Ouvrez le moniteur série pour observer l'état de la connexion et les messages de debug.
3. Assurez-vous que votre appareil audio Bluetooth est allumé et en mode appairage.
4. L'ESP32 devrait se connecter automatiquement à l'appareil audio Bluetooth et commencer à diffuser de l'audio.

### Dépannage

- **Pas de son** : Assurez-vous que la source audio analogique est correctement connectée aux broches GPIO 36 et GPIO 37 et fournit un signal valide.
- **Problèmes de connexion** : Vérifiez l'adresse MAC Bluetooth et assurez-vous que l'appareil est en mode appairage. Consultez le moniteur série pour d'éventuels messages d'erreur.
- **Problème de compilation** : il est possible que le programme soit trop lourd pour la carte, il faut donc modifier les partitions. Pour cela, dans l'IDE Arduino, rendez vous dans Tools > "Partition Scheme" > Huge APP.

## bt_music_sink

### Fonctionnalités

- Réception d'audio via Bluetooth depuis un appareil appairé.
- Traitement et sortie de l'audio reçu via le DAC de l'ESP32.

### Pré-requis

- Carte de développement ESP32.
- Appareil audio Bluetooth (smartphone, tablette, etc.) capable d'envoyer de l'audio via A2DP.

### Configuration Matérielle

1. Connectez un haut-parleur ou un casque à la broche GPIO 25 (DAC1) de l'ESP32.
2. Assurez-vous que votre appareil audio Bluetooth est détectable et prêt à être appairé.

### Configuration Logicielle

1. Importez les bibliothèques nécessaires :
   - `AudioTools.h`
   - `BluetoothA2DPSink.h`

### Explication du Code

#### Initialisation et Configuration

- **Configuration du Bluetooth** : Le récepteur Bluetooth A2DP est initialisé avec une fonction de rappel pour traiter les données audio reçues.

#### Traitement des Données Audio

La fonction `audio_data_callback` traite les échantillons audio reçus en les convertissant pour le DAC :

```cpp
void audio_data_callback(const uint8_t *data, uint32_t length) {
    // Traite les données audio reçues
    for (int i = 0; i < length; i += 2) {
        int16_t sample = ((int16_t)data[i + 1] << 8) | data[i];
        int8_t dac_value = sample >> 8; // Conversion à 8 bits pour le DAC
        dacWrite(25, dac_value + 128); // Sortie sur DAC1 (GPIO 25)
    }
}
```

#### Fonctions Setup et Loop

- **setup()** : Initialise le récepteur A2DP avec la fonction de rappel pour les données audio et démarre le service Bluetooth.
- **loop()** : La boucle principale est vide car toutes les opérations nécessaires sont gérées par la fonction de rappel et les bibliothèques.

```cpp
void setup() {
    // Initialiser le récepteur A2DP avec la fonction de rappel pour les données audio
    a2dp_sink.set_stream_reader(audio_data_callback);
    a2dp_sink.start("ESP32");
}

void loop() {
    // Boucle vide car tout se passe dans les bibliothèques et la fonction de rappel
}
```

### Utilisation

1. Compilez et téléversez le code sur votre ESP32.
2. Ouvrez le moniteur série pour observer l'état de la connexion et les messages de debug.
3. Assurez-vous que votre appareil audio Bluetooth est allumé et en mode de transmission audio.
4. L'ESP32 devrait se connecter automatiquement à l'appareil audio Bluetooth et commencer à recevoir et diffuser de l'audio via le DAC.

### Dépannage

- **Pas de son** : Assurez-vous que le haut-parleur ou le casque est correctement connecté à la broche GPIO 25 et que l'appareil audio Bluetooth envoie effectivement un signal audio.
- **Problèmes de connexion** : Vérifiez que le Bluetooth de l'ESP32 est activé et que l'appareil audio Bluetooth est en mode de transmission. Consultez le moniteur série pour d'éventuels messages d'erreur.
- **Problème de compilation** : il est possible que le programme soit trop lourd pour la carte, il faut donc modifier les partitions. Pour cela, dans l'IDE Arduino, rendez vous dans Tools > "Partition Scheme" > Huge APP.


## bt_scan_and_connect

### Fonctionnalités

- Découverte asynchrone des appareils Bluetooth à proximité.
- Connexion à un appareil Bluetooth spécifique.
- Communication bidirectionnelle via Bluetooth Serial (SPP).

### Pré-requis

- Carte de développement ESP32.
- Appareil Bluetooth capable de communiquer via Serial Bluetooth (SPP).

### Configuration Matérielle

- Aucune configuration matérielle spéciale n'est nécessaire pour cette application. Assurez-vous simplement que votre ESP32 est alimenté et prêt à être programmé.

### Configuration Logicielle

1. Installez les bibliothèques nécessaires :
   - `BluetoothSerial.h`
   - Incluez les bibliothèques standards C++ telles que `<map>`.

### Explication du Code

#### Initialisation et Configuration

- **Bluetooth Serial** : Initialisation du Bluetooth Serial avec le nom de l'appareil "ESP32test". La fonction de découverte asynchrone est utilisée pour rechercher des appareils Bluetooth à proximité.

#### Découverte et Connexion Bluetooth

La fonction `setup()` initialise la communication série, démarre la découverte asynchrone des appareils Bluetooth et tente de se connecter au premier appareil trouvé ayant des services disponibles :

```cpp
void setup() {
  Serial.begin(115200);
  if (!SerialBT.begin("ESP32test", true)) {
    Serial.println("========== serialBT failed!");
    abort();
  }

  Serial.println("Starting discoverAsync...");
  BTScanResults *btDeviceList = SerialBT.getScanResults();
  if (SerialBT.discoverAsync([](BTAdvertisedDevice *pDevice) {
        Serial.printf(">>>>>>>>>>>Found a new device asynchronously: %s\n", pDevice->toString().c_str());
      })) {
    delay(BT_DISCOVER_TIME);
    Serial.print("Stopping discoverAsync... ");
    SerialBT.discoverAsyncStop();
    Serial.println("discoverAsync stopped");
    delay(5000);
    if (btDeviceList->getCount() > 0) {
      BTAddress addr;
      int channel = 0;
      Serial.println("Found devices:");
      for (int i = 0; i < btDeviceList->getCount(); i++) {
        BTAdvertisedDevice *device = btDeviceList->getDevice(i);
        Serial.printf(" ----- %s  %s %d\n", device->getAddress().toString().c_str(), device->getName().c_str(), device->getRSSI());
        std::map<int, std::string> channels = SerialBT.getChannels(device->getAddress());
        Serial.printf("scanned for services, found %d\n", channels.size());
        for (auto const &entry : channels) {
          Serial.printf("     channel %d (%s)\n", entry.first, entry.second.c_str());
        }
        if (channels.size() > 0) {
          addr = device->getAddress();
          channel = channels.begin()->first;
        }
      }
      if (addr) {
        Serial.printf("connecting to %s - %d\n", addr.toString().c_str(), channel);
        SerialBT.connect(addr, channel, sec_mask, role);
      }
    } else {
      Serial.println("Didn't find any devices");
    }
  } else {
    Serial.println("Error on discoverAsync f.e. not working after a \"connect\"");
  }
}
```

### Utilisation

1. Compilez et téléversez le code sur votre ESP32.
2. Ouvrez le moniteur série pour observer l'état de la connexion et les messages de debug.
3. Assurez-vous que votre appareil Bluetooth est allumé et en mode appairage ou recherche.
4. L'ESP32 devrait détecter les appareils Bluetooth à proximité, essayer de se connecter et commencer la communication bidirectionnelle.

### Dépannage

- **Aucun appareil trouvé** : Assurez-vous que le Bluetooth de l'appareil est activé et détectable.
- **Connexion échouée** : Vérifiez que l'appareil est en mode appairage et qu'il accepte les connexions entrantes. Consultez le moniteur série pour d'éventuels messages d'erreur.
- **Pas de communication** : Assurez-vous que l'appareil Bluetooth est capable de communiquer via le Serial Bluetooth (SPP).
- **Problème de compilation** : il est possible que le programme soit trop lourd pour la carte, il faut donc modifier les partitions. Pour cela, dans l'IDE Arduino, rendez vous dans Tools > "Partition Scheme" > Huge APP.


## wifi_interface

### Fonctionnalités

- Crée un point d'accès Wi-Fi avec l'ESP32.
- Fournit une interface web pour scanner et se connecter à des appareils Bluetooth. (scan et connexion indisponibles sur ce code)
- Utilise WebSocket pour gérer les connexions en temps réel.

### Pré-requis

- Carte de développement ESP32.

### Configuration Logicielle

1. Installez les bibliothèques nécessaires :
   - `WiFi.h`
   - `ESPAsyncWebServer.h`
   - `AsyncTCP.h`

2. Définissez les informations d'identification Wi-Fi dans le code :
   ```cpp
   const char* ssid = "ESP32_Access_Point";
   const char* password = "123456789";
   ```

### Explication du Code

#### Initialisation et Configuration

Le code configure l'ESP32 pour qu'il fonctionne en mode point d'accès Wi-Fi et héberge un serveur web asynchrone. Une page HTML est servie pour permettre la connexion à des appareils Bluetooth.

```cpp
void setup() {
    Serial.begin(115200);

    // Configure l'ESP32 en mode point d'accès avec une IP fixe
    WiFi.softAP(ssid, password);
    IPAddress local_ip(192, 168, 0, 1);
    IPAddress gateway(192, 168, 0, 1);
    IPAddress subnet(255, 255, 255, 0);
    WiFi.softAPConfig(local_ip, gateway, subnet);

    Serial.println("WiFi Access Point started");
    Serial.print("IP Address: ");
    Serial.println(WiFi.softAPIP());

    // Sert le contenu HTML à la racine
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send_P(200, "text/html", html_page);
    });

    // Démarre le serveur
    server.begin();
    Serial.println("HTTP server started");
}
```

#### Interface Web et Gestion des Requêtes

La page HTML incluse permet à l'utilisateur de scanner et de se connecter à des appareils Bluetooth à proximité.

```html
const char html_page[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>Bluetooth Device Connect</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function scanBluetooth() {
      fetch('/scan')
        .then(response => response.json())
        .then(data => {
          let devicesList = document.getElementById('devices');
          devicesList.innerHTML = '';
          data.devices.forEach(device => {
            let option = document.createElement('option');
            option.value = device.address;
            option.innerText = device.name + ' (' + device.address + ')';
            devicesList.appendChild(option);
          });
        });
    }
  </script>
</head>
<body>
  <h2>Connect to Bluetooth Device</h2>
  <form action="/connect" method="get">
    Bluetooth Device Address: 
    <select id="devices" name="bt_device">
      <option value="" disabled selected>Select a device</option>
    </select><br>
    <input type="submit" value="Connect">
  </form>
  <button onclick="scanBluetooth()">Scan Bluetooth Devices</button>
</body>
</html>
)rawliteral";
```

#### Boucle Principale

La boucle principale gère les connexions WebSocket pour la communication en temps réel.

```cpp
void loop() {
    // Gestion des connexions WebSocket
    ws.cleanupClients();
}
```

### Utilisation

1. Compilez et téléversez le code sur votre ESP32.
2. Connectez-vous au point d'accès Wi-Fi nommé "ESP32_Access_Point" avec le mot de passe "123456789".
3. Ouvrez un navigateur web et allez à l'adresse `192.168.0.1`.
4. Utilisez l'interface web pour scanner et vous connecter à des appareils Bluetooth à proximité.

### Dépannage

- **Pas de connexion Wi-Fi** : Vérifiez que l'ESP32 est correctement alimenté et que les informations d'identification Wi-Fi sont correctes.
- **Page web non accessible** : Assurez-vous que l'ESP32 a démarré le point d'accès et que l'adresse IP est correcte.
- **Aucun appareil Bluetooth trouvé** : Assurez-vous que les appareils Bluetooth sont allumés et en mode détectable.
- **Problème de compilation** : il est possible que le programme soit trop lourd pour la carte, il faut donc modifier les partitions. Pour cela, dans l'IDE Arduino, rendez vous dans Tools > "Partition Scheme" > Huge APP.


## esp-idf-a2dp-source.zip

Ce document est une copie de [ce dépôt](https://github.com/nopnop2002/esp-idf-a2dp-source/tree/main). C'est une piste que nous n'avons pas encore eu l'occasion d'explorer mais qui demande plus d'expérience qu'un simple programme Arduino.

Il faut pour cela installer [esp-idf](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html) et suivre les instructions présentes dans le dépôt.

Ressources additionnelles:
- [dépôt officiel espressif](https://github.com/espressif/esp-idf/tree/master/examples/bluetooth/bluedroid/classic_bt/a2dp_source);
- [esp32 a2dp source](https://github.com/schreibfaul1/ESP32_A2DP_Source/tree/master) par schreibfaul1;