#include "AudioTools.h"
#include "BluetoothA2DPSink.h"

// Déclaration de l'objet Bluetooth A2DP Sink
AnalogAudioStream out;
BluetoothA2DPSink a2dp_sink(out);

// Fonction de rappel pour le traitement de l'audio reçu
void audio_data_callback(const uint8_t *data, uint32_t length) {
    // Traite les données audio reçues
    for (int i = 0; i < length; i += 2) {
        int16_t sample = ((int16_t)data[i + 1] << 8) | data[i];
        int8_t dac_value = sample >> 8; // Conversion à 8 bits pour le DAC
        dacWrite(25, dac_value + 128); // Sortie sur DAC1 (GPIO 25)
    }
}

void setup() {
    // Initialiser le récepteur A2DP avec la fonction de rappel pour les données audio
    a2dp_sink.set_stream_reader(audio_data_callback);
    a2dp_sink.start("ESP32");
}

void loop() {
    // Boucle vide car tout se passe dans les bibliothèques et la fonction de rappel
}
