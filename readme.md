# Guitar Amp
Ce projet CODEVSI a pour objectif principal la création d’un amplificateur de guitare portatif, il s’inscrit dans la continuité du projet n°20 de l’année 2022 - 2023.
Le rôle de notre amplificateur audio est d’amplifier un signal sonore provenant d’une guitare ou d’un téléphone afin de restituer un signal d’amplitude plus élevé. Il s’agit donc de créer un circuit électronique capable d'amplifier un signal sonore tout en respectant la contrainte de la portabilité. Nous nous intéresserons aussi à la création d’un module Bluetooth qui permettra de lier un appareil électronique à notre amplificateur.
Il nous a donc fallu nous pencher sur les travaux qui ont été établis auparavant, effectuer des recherches pour trouver des pistes d’améliorations, conceptualiser des maquettes et faire des tests de validation.


## 2023

Dossier contenant les recherches du projet de l'année 2022-2023.
- *_RapportProjet20.pdf* : rapport technique;
- *codevsi.py* : simulations ayant servi à déterminer la valeur de certains composants. Le code source se trouve [ici](https://colab.research.google.com/drive/1L3QKXWJg7lOb-JVY2tUovwxm2iTMuSbz);
- *Codev-test-pré-ampli.zip* : archive contenant des fichiers nécessaires aux simulations;
- *V2_CODEVSI_CdC_modele_v2023__copie_.pdf* : cahier des charges fonctionnel;

## 2024

Dossier contenant les recherches du projet de l'année 2023-2024.
- *CDCfProjet03.pdf* : cahier des charges fonctionnel;
- *RapportProjet03.pdf* : rapport technique;
- *PSpice_ampli_ua741.zip* : Projet Capture CIS Lite simulant le comportement du préamplificateur;
- [*bluetooth*](2024/bluetooth/) : codes Arduino pour la connexion Bluetooth;
- *docs* : documents techniques des composants utilisés;

