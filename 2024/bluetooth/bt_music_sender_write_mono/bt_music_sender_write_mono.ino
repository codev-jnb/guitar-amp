#include "BluetoothA2DPSource.h"
#include <driver/adc.h>
#include "esp_adc_cal.h"

BluetoothA2DPSource ad2p_source;

esp_bd_addr_t peer_address = {0x4C, 0x87, 0x5D, 0xAB, 0x51, 0x73}; // Adresse MAC d'un casque audio, à modifier en fonction du périphérique

// ADC channel for analog input
#define ADC_CHANNEL ADC1_CHANNEL_0 // GPIO 36

// Number of samples per frame
#define SAMPLES_PER_FRAME 256

// ADC read resolution and attenuation
#define ADC_READ_RESOLUTION ADC_WIDTH_BIT_12
#define ADC_ATTENUATION ADC_ATTEN_DB_11

// Buffer to hold the ADC samples
int16_t adcBuffer[SAMPLES_PER_FRAME * 2]; // *2 for stereo

// ADC calibration characteristics
esp_adc_cal_characteristics_t *adc_chars;

// callback 
int32_t get_sound_data(Frame *data, int32_t frameCount) {
    for (int i = 0; i < frameCount; i++) {
        // Read from ADC
        uint32_t adc_reading = adc1_get_raw(ADC_CHANNEL);

        // Convert the raw ADC reading to voltage in mV
        uint32_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);

        // Convert the voltage to a sample value
        int16_t sample = (voltage - 1800) * 16; // Center around 0 and amplify

        // Fill both channels with the same sample (mono to stereo)
        adcBuffer[i * 2] = sample;      // Left channel
        adcBuffer[i * 2 + 1] = sample;  // Right channel
    }

    // Copy the buffer to the frame data
    memcpy(data, adcBuffer, frameCount * 4); // 4 bytes per frame (2 channels * 2 bytes)

    return frameCount;
}




void setup() {
  Serial.begin(115200);
  a2dp_source.set_auto_reconnect(true);
  // Allocate memory for the ADC calibration characteristics
  adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));

  // Characterize ADC
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTENUATION, ADC_READ_RESOLUTION, 1100, adc_chars);

  a2dp_source.start("ESP32", get_sound_data);
  a2dp_source.set_volume(100);
  a2dp_source.connect_to(peer_address);
  a2dp_source.write_data(data);
  }


void loop() {
}
