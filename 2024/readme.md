# 2024

## ampli

Dossier contenant les ressources ayant servi à la réalisation de l'ampli. Cela comprend les schémas des circuits, le Projet Capture CIS Lite simulant le comportement du préamplificateur ainsi que la copie .pdf du site internet de notre référence pour la solution avec le LM386.

## bluetooth

Dans ce dossier vous trouverez les codes permettant d'établir une connection Bluetooth a2dp (audio), réalisés sur l'*IDE Arduino*.
Ils ont nécessité les bibliothèques [BluetoothA2DP](https://github.com/pschatzmann/ESP32-A2DP) et [Audiotools](https://github.com/pschatzmann/arduino-audio-tools).

Détails des programmes [ici](2024/bluetooth/).

## docs

Ce dossier contient les documentations des différents composants électroniques ayant servi:
- *78L05.pdf* : régulateur de tension
- *A741.pdf* : AOP
- *jack_3.5.pdf* : jack femelle 3.5mm 3 pôles
- *jack_6.35.pdf* : jack femelle 6.35mm mono
- *lm386.pdf* : amplificateur audio
- *lm2576.pdf* : convertisseur DC-DC
- *mbr360.pdf* : diode de redressement schottky
- 

Ainsi que les modèles pspices utilisables dans Capture CIS / OrCAD.

## plannings

Dossier contenant les 2 plannings (prévisionnel et réalisé) de notre projet

## CDCfProjet03.pdf

Cahier des charges fonctionnel.

## RapportProjet03.pdf

Rapport technique.